/* src/include/port/haiku.h */

/* Haiku doesn't have all the required getrusage fields */
//#undef HAVE_GETRUSAGE

/* Haiku doesn't implement shared memory as needed*/
struct shmid_ds
{
	int			dummy;
	int			shm_nattch;
};

int			shmdt(char *shmaddr);
int		   *shmat(int memId, int m1, int m2);
int			shmctl(int shmid, int flag, struct shmid_ds * dummy);
int			shmget(int memKey, int size, int flag);
